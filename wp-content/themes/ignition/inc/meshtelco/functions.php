<?php
add_action('wp_ajax_mesh_plan_request', array('Mesh_Post', 'mesh_plan_request') );
add_action('wp_ajax_nopriv_mesh_plan_request', array('Mesh_Post', 'mesh_plan_request') );
add_action('wp_ajax_mesh_enquiry', array('Mesh_Post', 'mesh_enquiry') );
add_action('wp_ajax_nopriv_mesh_enquiry', array('Mesh_Post', 'mesh_enquiry') );
add_filter('wp_mail_content_type', array('Mesh_Post', 'mesh_mail_content_type') );


add_action( 'wp_enqueue_scripts', 'meshtelco_enqueue_scripts' );

function meshtelco_enqueue_scripts() {
    wp_enqueue_script('jquery');
    wp_enqueue_script('meshtelco_scripts', get_template_directory_uri() . '/js/meshtelco/scripts.js', array(), '1.0.0', true);
}
class Mesh_Post {
    const WEBHOOK_URL = 'https://hooks.slack.com/services/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';
    const SLACK_CHANNEL = '#website-leads';
    const SLACK_BOT = 'meshbot';

    public static function mesh_enquiry() {
        if (isset($_POST['url'][0]) && isset($_POST['name'][0]) && isset($_POST['phone'][0]) && isset($_POST['email'][0])) {
            $payload_fallback = sprintf("General enquiry\n Name: %s\n Phone: %s\n Email: %s\n URL: %s\n ", $_POST['name'], $_POST['phone'], $_POST['email'], $_POST['url']);

            $data = [
                'Date' => date('d/m/Y h:ia e'),
                'Name' => $_POST['name'],
                'Phone' => $_POST['phone'],
                'Email' => $_POST['email']
            ];

            if (isset($_POST['business'])) {
                $payload_fallback .= sprintf("Business: %s \n", $_POST['business']);
                $data['Business'] = $_POST['business'];
            }

            if (isset($_POST['message'])) {
                $payload_fallback .= sprintf("Message: %s \n", $_POST['message']);
                $data['Message'] = $_POST['message'];
            }

            $data['URL'] = $_POST['url'];
            $subject = 'General enquiry';

            self::send_slack_request($subject, $data, $payload_fallback);
            $mail = self::send_email($subject, $data);

            if ($mail) {
                echo 'success';
            } else {
                status_header( 400 );
            }

        } else {
            status_header( 400 );
        }
        exit();
    }

    public static function mesh_plan_request() {

        if (isset($_POST['planid']) && isset($_POST['name'][0]) && isset($_POST['address'][0]) && isset($_POST['phone'][0]) && isset($_POST['email'][0])) {

            $plan = get_post((int)$_POST['planid']);
            if (!$plan)
                return status_header(400);

            $parent_id = wp_get_post_parent_id( $plan->ID );
            if (!$parent_id)
                return status_header(400);
            $connection = get_post($parent_id);
            $plan_name = sprintf('%s - %s', $connection->post_title, $plan->post_title);

            $data = [
                'Date' => date('d/m/Y h:ia e'),
                'Name' => $_POST['name'],
                'Phone' => $_POST['phone'],
                'Email' => $_POST['email'],
                'Address' => $_POST['address'],
                'Plan' => $plan_name,
            ];
            $payload_fallback = sprintf("Internet enquiry - %s\n Name: %s\n Phone: %s\n Email: %s\n Address: %s", $plan_name, $_POST['name'], $_POST['phone'], $_POST['email'], $_POST['address']);

            $subject = "Internet enquiry - " . $plan_name;
            self::send_slack_request($subject, $data, $payload_fallback);
            $mail = self::send_email($subject, $data);

            if ($mail) {
                echo 'success';
            } else {
                status_header( 400 );
            }
        }
        else {
            status_header( 400 );
        }
        exit();

    }
    public static function send_email($subject, $data) {

        $to = get_option('admin_email');
        $headers = 'From: "MeshTelco Website" <info@meshtelco.com.au>';

        ob_start();
        foreach ($data as $k => $v) {
            printf('<b>%s</b><br />%s', $k, wpautop($v));
        }

        $message = ob_get_contents();

        ob_end_clean();

        return wp_mail($to, $subject, $message, $headers);
    }

    public static function send_slack_request($subject, $data, $fallback = '') {
        $fields = [];
        foreach ($data as $k => $v) {
            $fields []= [
                "title" => $k,
                "value" => $v,
                "short" => false
            ];
        }
        $payload = [
            'channel' => self::SLACK_CHANNEL,
            'username' => self::SLACK_BOT,
            'attachments' => [
               [
                   'fallback' => $fallback,
                   "color" => "#4F2580",
                   "title" => $subject,
                   "fields" => $fields,
                   "ts" => time()
               ]
           ]
        ];
        try {
            wp_remote_post(self::WEBHOOK_URL, [ 'method' => 'POST', 'body' => [ 'payload' => json_encode($payload) ] ]);
        } catch (\Exception $e) {
        }

    }

    public static function mesh_mail_content_type() {
        return 'text/html';
    }
}
?>
