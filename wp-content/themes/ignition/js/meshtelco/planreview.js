var planDetails, planColor, planSignupLoc;
$(document).ready(function(){

    //binds the position of the info box to the question mark icon
    $('.icon-question-sign').click(function(){
        var questionPos = $(this).offset();
        var alertPosX = questionPos.left + 20;
        var alertPosY = questionPos.top - 20;
        $('.toast-top-right').css({'top': alertPosY +'px', 'left': alertPosX +'px', 'position' : 'absolute'});
    });

    //toggles whether the address is a business or not, and shows EFM and MBE tabs if so. If unchecked when EFM or MBE tab active, sets focus back to NBN tab.
    $('#form-business-address').click(function(){
        $('.businessplan').toggle(this.checked);
        if ( $('.businessplan').hasClass('ui-tabs-active')){
            $('.tab-nav li:first-child a').click();
        }
    });

    //the address check goes here. I've put in a check to see whether it's blank but will also need to validate address
    //$('#address-check').click(function(){
        //if( $('#address-field').val() ) {
        //	$('.tabs').removeClass('hidden');
            //$('.alert-success').removeClass('hidden');
        //}else{
            //SEMICOLON.widget.notifications( jQuery('#address-message') );
        //}
    //});

    //Proceeds to modem selection as soon as a plan button is pressed, store which plan at this point
    $('.pricing-action .plan-select').click(function(e){

        $('#planSignup').removeClass('hidden');
        planDetails = $(this).parent().parent().html();
        planColor = $(this).parent().parent().attr("class");
        $('#planSignup .pricing-box').html(planDetails);
        $('#planSignup .pricing-box .pricing-action').remove();
        $('#planSignup .pricing-box').removeClass("nbn adsl efm mbe life");
        $('#planSignup .pricing-box').addClass(planColor);

        $('#planid').val( $(this).data('planid') );
        //$('#modemSelection<?php //echo ($connection_tab_link);?>').removeClass('hidden');
    });

    //proceeds to signup as soon as a modem type is selected. Uses ScrollTop because input fields and anchors don't play nicely.
    $('.modem-selector input').click(function(){
        $('#planSignup').removeClass('hidden');
        planSignupLoc = $('#planSignup').offset().top;
        $('html,body').animate({
            scrollTop:$("#planSignup").offset().top},1000);
    });

    //Signup field check to see if all fields are filled in
    $('#signup-step1 .submit').click(function(){

        if( $('#name-field').val()  && ($('#email-field').val().indexOf("@") > 0) && $('#phone-field').val() ) {
            var data = $('#planSelector').serialize();
            $.post('/plan-request', data, function( response ) {
              $('.form-process').fadeTo('fast',1, function(){
                  $('#signup-step1').fadeOut(1000).promise().done(function(){
					  hj('formSubmitSuccessful');
                      var emailAddress = $('#email-field').val();
                      $('#signup-step1').addClass('hidden');
                      $('#signup-step2').removeClass('hidden');
                      $('#emailAddress').html(emailAddress);
                  });
              });

            });

        }else{
            SEMICOLON.widget.notifications( jQuery('#details-message') );
			hj('formSubmitFailed');
        }
    });

    //$('#signup-step2 .button').click(function(){
        //$('#signup-step2').addClass('hidden');
        //$('#signup-step3').removeClass('hidden');
    //});


});
