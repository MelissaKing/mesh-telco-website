jQuery(document).ready(function() {
    jQuery('.cta-form, .contact-form').on('submit', function(e) {
        e.preventDefault();
        var form = jQuery(this);
        var data = form.serialize();
        var button = jQuery('button', form);
        button.html('Sending...').attr('disabled', 'disabled');
        jQuery.post('/general-enquiry', data + '&url=' + location.href, function( response ) {
            form.html('<div class="cta-response"><h4>Thank you for your enquiry!</h4><p>We will be in touch shortly to discuss your requirements. For more urgent matters, please call us on 1300 080 820.</p></div>');
			hj('formSubmitSuccessful');
        });
    });
});
