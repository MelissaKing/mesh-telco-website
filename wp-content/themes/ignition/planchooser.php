<?php /**
 * Template Name: Planchooser
 *
 * @package WordPress
 * @subpackage ignition-child
 * @since Twenty Fourteen 1.0
 */	
?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Plycode" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/planstyle.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/animate.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->

	<!-- External JavaScripts
	============================================= -->
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/plugins.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			
			//binds the position of the info box to the question mark icon
			$('.icon-question-sign').click(function(){
				var questionPos = $(this).offset();
				var alertPosX = questionPos.left + 20;
				var alertPosY = questionPos.top - 20; 
				$('.toast-top-right').css({'top': alertPosY +'px', 'left': alertPosX +'px', 'position' : 'absolute'});
			});
			
			//toggles whether the address is a business or not, and shows EFM and MBE tabs if so. If unchecked when EFM or MBE tab active, sets focus back to NBN tab.
			$('#form-business-address').click(function(){
				$('.businessplan').toggle(this.checked);
				if ( $('.businessplan').hasClass('ui-tabs-active')){
					$('.tab-nav li:first-child a').click(); 
				}
			});
			
			//the address check goes here. I've put in a check to see whether it's blank but will also need to validate address
			$('#address-check').click(function(){
				if( $('#address-field').val() ) {
					$('.tabs').removeClass('hidden');
					$('.alert-success').removeClass('hidden');
				}else{
					SEMICOLON.widget.notifications( jQuery('#address-message') );
				}
			});
			
			//Proceeds to modem selection as soon as a plan button is pressed, store which plan at this point
			$('.pricing-action .button').click(function(){
				$('#modemSelection<?php echo ($connection_tab_link);?>').removeClass('hidden');
				$('#planSignup').removeClass('hidden');
				var planDetails = $(this).parent().parent().html();
				var planColor = $(this).parent().parent().attr("class");
				$('#planSignup .pricing-box').html(planDetails);
				$('#planSignup .pricing-box .pricing-action').remove();
				$('#planSignup .pricing-box').removeClass("nbn adsl efm mbe life");
				$('#planSignup .pricing-box').addClass(planColor);
			});
			
			//proceeds to signup as soon as a modem type is selected. Uses ScrollTop because input fields and anchors don't play nicely.
			$('.modem-selector input').click(function(){
				$('#planSignup').removeClass('hidden');
				var planSignupLoc = $('#planSignup').offset().top;
				$('html,body').animate({
					scrollTop:$("#planSignup").offset().top},1000);
			});
			
			//Signup field check to see if all fields are filled in
			$('#signup-step1 .button').click(function(){
				if( $('#name-field').val()  && ($('#email-field').val().indexOf("@") > 0) && $('#phone-field').val() ) {
					$('#signup-step1').addClass('hidden');
					$('#signup-step2').removeClass('hidden');
				}else{
					SEMICOLON.widget.notifications( jQuery('#details-message') );
				}
			});
			
			$('#signup-step2 .button').click(function(){
				var emailAddress = $('#email-field').val();
				$('#emailAddress').html(emailAddress);
				$('#signup-step2').addClass('hidden');
				$('#signup-step3').removeClass('hidden');
			});
		});
    </script>

	<!-- Document Title
	============================================= -->
	<title>Mesh Plan Chooser</title>
</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">
		
        
        <!-- Hidden Error Messages
		============================================= -->
        <div id="address-message" data-notify-position="top-full-width" data-notify-type="error" data-notify-msg="<i class='icon-remove-sign'></i> Whoops! You need to enter your address!"></div>
        <div id="details-message" data-notify-position="top-full-width" data-notify-type="error" data-notify-msg="<i class='icon-remove-sign'></i> Please enter all your details!"></div>


		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
                	<h2 class="center meshred">Let’s get started</h2>
                	<h3 class="center">1. Enter your address</h3>
						<form id="conditional-form" action="#" method="post" class="nobottommargin">

							<div class="col_full address-checker clearfix">
								<input type="text" class="sm-form-control required nomargin col_three_fourth" id="address-field" name="address-field" placeholder="Enter your address">
                                <a href="#tab-plans" data-scrollto="#tab-plans" name="address-check" id="address-check" class="nomargin button arrow col_one_fourth">Find plans</a>
                             </div>
                             <div class="col_full clearfix">
                                <input type="checkbox" class="pull-left rightmargin-sm" name="form-business-address" id="form-business-address"/><label for="form-business-address" class="pull-left">This is a business address &nbsp;</label><i class="icon-question-sign icon-lg" data-notify-type="info" data-notify-msg="<i class=icon-info-sign></i> Some types of connection are only available at business addresses." onclick="SEMICOLON.widget.notifications(this); return false;"></i>
							</div>
                            
                            <div class="alert alert-success center hidden"><h4 class="nobottommargin"><i class="icon-ok-sign icon-lg"></i> Good news! The following plans are available at your address:</h4></div>
                                <div class="tabs tabs-alt  tabs-tb clearfix tabs-bordered hidden" id="tab-plans">
        
                                    <ul class="tab-nav clearfix">
                                    
                                     <?php 
									
									//Sets up query to get connection types, and then generate the tabs based on the results (ie, adsl, etc)
									$connection_query = new WP_Query();
									$connection = get_page_by_title('Plans');
									$args = array(
										'post_type' => 'page',
										'post_parent' => $connection->ID,
										'ignore_sticky_posts' => true,
										'fields' => 'ids',
										'order' =>'ASC',
										'orderby' => 'date'
									);
									$connection_pages = $connection_query->query($args);	
									$connection_children = get_page_children( $connection -> ID, $connection_pages );
									
									foreach ($connection_pages as $connection) {
				
										$connection_type = get_the_title($connection);
										$connection_tab_link1 = strtolower(str_replace(" ", "-", $connection_type));
										$connection_tab_link = preg_replace("/[^a-zA-Z0-9]+/", "", $connection_tab_link1);
										?><li 
										<?php 
										if ((strpos($connection_type, 'EFM')!==false ) || (strpos($connection_type, 'MBE')!==false )){
											?>class="businessplan" style="display:none;"
											<?php }?>>
                                            <a href="#tab-<?php echo ($connection_tab_link);?>"
                                            class="<?php switch (true){
												case stristr($connection_type,"adsl"):
													echo ("adsl");
													break;
												case stristr($connection_type,"mbe"):
													echo ("mbe");
													break;
												case stristr($connection_type,"efm"):
													echo ("efm");
													break;
												case stristr($connection_type,"nbn"):
													echo ("nbn");
													break;
												case stristr($connection_type,"life"):
													echo ("life");
													break;
											}?>"
                                            ><?php echo ($connection_type);?></a></li>
									<?php }?>  
                                    </ul>
        <?php //print_r($connection_pages);?>
                                    <div class="tab-container">
        								<h3 class="center">2. Select a plan</h3>
                                        
                                        <?php foreach ($connection_pages as $connection) {
										$connection_type = get_the_title($connection);
										$connection_tab_link1 = strtolower(str_replace(" ", "-", $connection_type));
										$connection_tab_link = preg_replace("/[^a-zA-Z0-9]+/", "", $connection_tab_link1);
										?><div class="tab-content clearfix <?php switch (true){
												case stristr($connection_type,"adsl"):
													echo ("adsl");
													break;
												case stristr($connection_type,"mbe"):
													echo ("mbe");
													break;
												case stristr($connection_type,"efm"):
													echo ("efm");
													break;
												case stristr($connection_type,"nbn"):
													echo ("nbn");
													break;
												case stristr($connection_type,"life"):
													echo ("life");
													break;
											}?>" id="tab-<?php echo ($connection_tab_link);?>">
										
                                        
                                        	<div class="row">
                                            	<div class="pricing bottommargin clearfix">
                                                <?php
												
												$plan_query = new WP_Query();
												$args1 = array(
													'post_type' => 'page',
													'post_parent' => $connection,
													'ignore_sticky_posts' => true,
													'fields' => 'ids',
													'order' =>'ASC',
													'orderby' => 'date'
												);
												$plan_pages = $plan_query->query($args1);	


												foreach ($plan_pages as $plan) {
													
													$plan_title =  get_the_title($plan);
													$plan_monthly_cost  = get_field("monthly_cost", $plan);
													$plan_setup_cost =  get_field("setup_cost", $plan);
													$plan_data = get_field("data", $plan);
													$plan_terms = get_field("terms", $plan);
													$plan_min_upload = get_field("min_upload", $plan);
													$plan_min_download = get_field("min_download_speed", $plan);
													$plan_max_upload = get_field("max_upload_speed", $plan);
													$plan_max_download = get_field("max_download_speed", $plan);
													$plan_pds = get_field("pds_link", $plan);
													
													?>
                                                    <div class="col-md-4">
                                                        <div class="pricing-box">
                                                            <div class="pricing-title">
                                                                <h3><?php echo($plan_title);?></h3>
                                                            </div>
                                                            <div class="pricing-price">
                                                                <span class="price-unit">&dollar;</span><?php echo($plan_monthly_cost);?><span class="price-tenure">/mo</span>
                                                            </div>
                                                            <div class="pricing-mincost">
                                                            	<span><strong>Available terms:</strong> </span><br/>
                                                                
                                                                <?php foreach ($plan_terms as $term){
																	echo ($term . " months ");
																	?><span>min cost $<?php echo($plan_setup_cost + ($plan_monthly_cost * $term));?>, </span><br/><?php
																}?>
                                                            </div>
                                                            <div class="pricing-features">
                                                                <ul>
                                                                    <li><strong><?php echo($plan_data);?></strong> Data</li>
                                                                    <li><strong><?php echo($plan_max_download);?>/<?php echo($plan_max_upload);?></strong> Mbps</li>
                                                                    <li><a href="<?php echo($plan_pds['url']);?>">Product Disclosure Statement</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="pricing-action">
                                                                <a href="#modemSelection" data-scrollto="#modemSelection" class="button" data-plantype="<?php echo($connection_type . " " . $plan_data. " " . $plan_max_upload);?>">Select plan</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php }?> 
                                                </div>
                                            </div>
                                        </div>
                                        <?php }?>
                                        <div id="modemSelection" class="row hidden">
                                            <h3 class="center"> 3. Select a modem</h3>
                                            
                                            <div class="row modem-selector bottommargin-sm">
                                                <div class="alert alert-info col_three_fourth nomargin">
                                                    <input type="radio" name="modem-option" id="modem-option1"/><label for="modem-option1">Awesome nifty NBN ready router</label>
                                                </div>
                                                <div class="alert alert-dark col_one_fourth col_last nomargin">
                                                    <p><sup>$</sup>79</p>
                                                </div>
                                            </div>
                                            <div class="row modem-selector bottommargin-sm">
                                                <div class="alert alert-info col_three_fourth nomargin">
                                                    <input type="radio" name="modem-option" id="modem-option2"/><label for="modem-option2">BYO Modem</label>
                                                </div>
                                                <div class="alert alert-dark col_one_fourth col_last nomargin">
                                                    <p><sup>$</sup>0</p>
                                                </div>
                                            </div>
                                        </div>
                                       	<div id="planSignup" class="row hidden">
                                            	<h3 class="center">4. Confirm and sign up</h3>
                                            	<div class="alert-dark alert nomargin nopadding clearfix">
                                                    <div class="col_half nobottommargin">
                                                        <div class="pricing nobottommargin clearfix">
                                                            <div class="col-full notopmargin">
                                                                   <div class="pricing-box nomargin">
                                                                        <div class="pricing-title">
                                                                            <h3>Starter</h3>
                                                                        </div>
                                                                        <div class="pricing-price">
                                                                            <span class="price-unit">&dollar;</span>7<span class="price-tenure">/mo</span>
                                                                        </div>
                                                                        <div class="pricing-mincost">
                                                                            <span>min cost $500.40</span>
                                                                        </div>
                                                                        <div class="pricing-features">
                                                                            <ul>

                                                                                <li><strong>Full</strong> Access</li>
                                                                                <li><i class="icon-code"></i> Source Files</li>
                                                                                <li><strong>100</strong> User Accounts</li>
                                                                                <li><strong>1 Year</strong> License</li>
                                                                                <li><a href="pds.pdf">Product Disclosure Statement</a></li>
                                                                            </ul>
                                                                        </div>
                                                                        
                                                                    </div>
                                                              </div>
                                                          </div>
                                                    </div>
                                                    
                                                    <div class="col_half col_last nobottommargin topmargin-sm modal-padding">
                                                        <div id="signup-step1">
                                                            <h4 class="center">Enter your details</h4>
                                                            
                                                            <div class="clearfix bottommargin-sm">
                                                                <input type="text" class="sm-form-control required name" id="name-field" name="name-field" value="" placeholder="Name">
                                                            </div>
                                                            
                                                            <div class="clearfix bottommargin-sm">
                                                                <input type="email" class="sm-form-control required email" id="email-field" name="email-field" value="" placeholder="Email">
                                                            </div>
                                                            
                                                            <div class="clearfix bottommargin-sm">                                                            
                                                                <input type="phone" class="sm-form-control required phone" id="phone-field" name="phone-field" value="" placeholder="Phone">
                                                            </div>
                                                            
                                                            <div class="col_full" id="form-condition-submit">
                                                                <a class="button nomargin pull-right arrow dark">Review plan</a>
                                                            </div>
                                                        </div>
                                                        
                                                        <div id="signup-step2" class="hidden">
                                                            <h4>Confirm your details</h4>
                                                            
                                                            <dl>
                                                            	<dt>Name</dt>
                                                                <dd>Greg Hollander</dd>
                                                                
                                                                <dt>Email</dt>
                                                                <dd>GregH1@gmail.com</dd>
                                                                
                                                                <dt>Phone</dt>
                                                                <dd>0400 000 000</dd>
                                                                
                                                                <dt>Plan</dt>
                                                                <dd>NBN Lite</dd>
                                                                
                                                                <dt>Service address</dt>
                                                                <dd>Level 1, 123 Test st TESTVILLE VIC 3133</dd>
                                                            </dl>
                                                            <div class="col_full" id="form-condition-submit3">
                                                                <a class="button nomargin pull-right arrow dark">Confirm signup</a>
                                                            </div>
                                                        </div>
                                                        <div id="signup-step3" class="hidden">
                                                            <h4>Thanks for signing up!</h4>
                                                            
                                                            <p>Your order number is 100101010. A confirmation has been sent to <span id="emailAddress">your email address</span></p>
                                                            <p>A consultant will be in touch shortly to finalise your account and setup your connection.
Problems? Call us on 1300 000 000.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                  
                                </div>
                                    
                            </div>

						</form>



						<script>
							//jQuery(document).ready( function($){
//								var conditionValidator = $("#conditional-form").validate({
//									rules: {
//										'form-condition-5': {
//											required: true,
//											minlength: 20
//										}
//									}
//								});
//
//								$('#form-condition-1').bind('input propertychange', function() {
//									if( conditionValidator.element( "#form-condition-1" ) === true ) {
//										$('#form-condition-2-wrap').fadeIn();
//									} else {
//										$('#form-condition-2-wrap').fadeOut();
//									}
//								});
//
//								$('#form-condition-2').bind('input propertychange', function() {
//									if( conditionValidator.element( "#form-condition-2" ) === true ) {
//										$('#form-condition-3-wrap').fadeIn();
//									} else {
//										$('#form-condition-3-wrap').fadeOut();
//									}
//								});
//
//								$('#form-condition-3').bind('input propertychange', function() {
//									if( conditionValidator.element( "#form-condition-3" ) === true ) {
//										$('#form-condition-4-wrap').fadeIn();
//									} else {
//										$('#form-condition-4-wrap').fadeOut();
//									}
//								});
//
//								$('#form-condition-4').change( function() {
//									if( conditionValidator.element( "#form-condition-4" ) === true ) {
//										$('#form-condition-5-wrap').fadeIn();
//									} else {
//										$('#form-condition-5-wrap').fadeOut();
//									}
//								});
//
//								$('#form-condition-5').bind('input propertychange', function() {
//									if( conditionValidator.element( "#form-condition-5" ) === true ) {
//										$('#form-condition-submit').fadeIn();
//									} else {
//										$('#form-condition-submit').fadeOut();
//									}
//								});
//							});
						</script>


				</div>
			</div>
		</section><!-- #content end -->
	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/functions.js"></script>

</body>
</html>