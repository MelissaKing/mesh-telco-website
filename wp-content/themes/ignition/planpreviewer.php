<?php /**
 * Template Name: Planpreviewer
 *
 * @package WordPress
 * @subpackage ignition-child
 * @since Twenty Fourteen 1.0
 */
?>


	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/planstyle.css" type="text/css" />
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->

	<!-- External JavaScripts
	============================================= -->
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/plugins.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/meshtelco/planreview.js"></script>
	<script type="text/javascript">
		$(window).load(function(){
			$(".preloader").fadeTo('slow',0,function(){
				$(this).remove();
			});
		});
    </script>
    <?php
	//switches the connection type for classes, which change the colours of the pricing boxes and tabs
	function connectionType($connection_type){
		switch (true){
			case stristr($connection_type,"adsl"):
				echo ("adsl");
				break;
			case stristr($connection_type,"mbe"):
				echo ("mbe");
				break;
			case stristr($connection_type,"efm"):
				echo ("efm");
				break;
			case stristr($connection_type,"nbn"):
				echo ("nbn");
				break;
			case stristr($connection_type,"life"):
				echo ("life");
				break;
		}



	}?>

	<!-- Document Wrapper
	============================================= -->

	<div id="wrapper" class="clearfix" data-loader="2" data-animation-in="fadeIn" >

        <!-- Hidden Error Messages
		============================================= -->
        <div id="address-message" data-notify-position="top-full-width" data-notify-type="error" data-notify-msg="<i class='icon-remove-sign'></i> Whoops! You need to enter your address!"></div>
        <div id="details-message" data-notify-position="enquire-full-width" data-notify-type="error" data-notify-msg="<i class='icon-remove-sign'></i> Please enter all your details!"></div>


		<!-- Content
		============================================= -->
		<section id="planChooser">

			<div class="content-wrap">
            	<h2 class="center">Browse our plans</h2>
                <div class="preloader"></div>
				<div class="container clearfix">
							<!--<div class="col_full address-checker clearfix">
								<input type="text" class="sm-form-control required nomargin col_three_fourth" id="address-field" name="address-field" placeholder="Enter your address">
                                <a href="#tab-plans" data-scrollto="#tab-plans" name="address-check" id="address-check" class="nomargin button arrow col_one_fourth">Find plans</a>
                             </div>-->
                             <div class="col_full clearfix">
                                <input type="checkbox" class="pull-left rightmargin-sm" name="form-business-address" id="form-business-address"/><label for="form-business-address" class="pull-left">Also show me business plans &nbsp;</label><i class="icon-question-sign icon-lg" data-notify-type="info" data-notify-msg="<i class=icon-info-sign></i> Some types of connection are only available at business addresses." onclick="SEMICOLON.widget.notifications(this); return false;"></i>
							</div>

                            <!--<div class="alert alert-success center hidden"><h4 class="nobottommargin"><i class="icon-ok-sign icon-lg"></i> Good news! The following plans are available at your address:</h4></div>-->
                                <div class="tabs tabs-alt  tabs-tb clearfix tabs-bordered" id="tab-plans">

                                    <ul class="tab-nav clearfix">

                                     <?php

									//Sets up query to get connection types, and then generate the tabs based on the results (ie, adsl, etc)
									$connection_query = new WP_Query();
									$connection = get_page_by_title('Plans');
									$args = array(
										'post_type' => 'page',
										'post_parent' => $connection->ID,
										'ignore_sticky_posts' => true,
										'fields' => 'ids',
										'order' =>'ASC',
										'orderby' => 'menu_order'
									);
									$connection_pages = $connection_query->query($args);
									//loop to generate the tabs. Checks if it's a business connection and shows/hides depending on checkbox toggle up in jQuery
									foreach ($connection_pages as $connection) {

										$connection_type = get_the_title($connection);
										$connection_tab_link1 = strtolower(str_replace(" ", "-", $connection_type));
										$connection_tab_link = preg_replace("/[^a-zA-Z0-9]+/", "", $connection_tab_link1);
										$businessPlanClass = '';

										?>

										<li
                                        <?php if ((strpos($connection_type, 'EFM')!==false ) || (strpos($connection_type, 'MBE')!==false )){
											$businessPlanClass = 'businessplan';
											?>
                                             style="display:none;"
											<?php }?>

                                         class="<?php echo ($businessPlanClass);?>"
										>
                                            <a href="#tab-<?php echo ($connection_tab_link);?>"
                                            class="<?php connectionType($connection_type);?>"
                                            ><?php echo ($connection_type);?></a></li>
									<?php }

									// Checks the URL and highlights the relevant tab (ie, if it's MBE tab it checks the business plans box and highlights the first MBE tab)
									switch (true){
											case stristr($_SERVER['REQUEST_URI'],'nbn'):
											  ?>
											  <script type="text/javascript">
											  $(window).load(function(){
												  $('.nbn').click();
											  });
											  </script>
											  <?php
											  //echo ("nbn is an active page");
											  break;
										  case stristr($_SERVER['REQUEST_URI'],'adsl'):
											  ?>
											  <script type="text/javascript">
											  $(window).load(function(){
												  $('.adsl:first-of-type').click();
												  //alert("ADSL tab is active");
											  });
											  </script>
											  <?php
											  //echo ("adsl is an active page");
											  break;
										  case stristr($_SERVER['REQUEST_URI'],'mbe'):
											  ?>
											  <script type="text/javascript">
											  $(window).load(function(){
												  $('#form-business-address').prop('checked',true);
												  $('.businessplan').toggle(this.checked);
												  $('.mbe:first-of-type').click();
												 //alert("MBE tab is active");
											  });
											  </script>
											  <?php
											  //echo ("mbe is an active page");
											  break;
										  case stristr($_SERVER['REQUEST_URI'],'efm'):
											?>
											  <script type="text/javascript">
											  $(window).load(function(){
												  $('#form-business-address').prop('checked',true);
												  $('.businessplan').toggle(this.checked);
												  $('.efm:first-of-type').click();
												 //alert("EFM tab is active");
											  });
											  </script>
											  <?php
											  //echo ("efm is an active page");
											  break;

										  case (stristr($_SERVER['REQUEST_URI'],'life') && (stristr($connection_type,'life'))):
										  	?>
											  <script type="text/javascript">
											  $(window).load(function(){
												  $('.lifewireless').click();
											  });
											  </script>
											  <?php
											  //echo ("life is an active page");
											  break;
									}
									?>
                                    </ul>
                                    <div class="tab-container">
        								<h3 class="center">1. Select a plan</h3>

                                        <?php
										//Loop to generate the actual tab frames
										foreach ($connection_pages as $connection) {
										$connection_type = get_the_title($connection);
										$connection_tab_link1 = strtolower(str_replace(" ", "-", $connection_type));
										$connection_tab_link = preg_replace("/[^a-zA-Z0-9]+/", "", $connection_tab_link1);
										?><div class="tab-content clearfix <?php connectionType($connection_type);?>" id="tab-<?php echo ($connection_tab_link);?>">


                                        	<div class="row">
                                            	<div class="pricing bottommargin clearfix">
                                                <?php

												$plan_query = new WP_Query();
												$args1 = array(
													'post_type' => 'page',
													'post_parent' => $connection,
													'ignore_sticky_posts' => true,
													'fields' => 'ids',
													'order' =>'ASC',
													'orderby' => 'date'
												);
												$plan_pages = $plan_query->query($args1);
												//inner loop inside tab frame to fetch plan data and generate pricing boxes for each
												foreach ($plan_pages as $plan) {
                                                    $plan_id = get_the_id($plan);
													$plan_title =  get_the_title($plan);
													$plan_monthly_cost  = get_field("monthly_cost", $plan);
													$plan_setup_cost =  get_field("setup_cost", $plan);
													$plan_data = get_field("data", $plan);
													$plan_terms = get_field("terms", $plan);
													$plan_min_upload = get_field("min_upload_speed", $plan);
													$plan_min_download = get_field("min_download_speed", $plan);
													$plan_max_upload = get_field("max_upload_speed", $plan);
													$plan_max_download = get_field("max_download_speed", $plan);
													$plan_cis = get_field("cis_link", $plan);

													?>
                                                    <div class="col-md-<?php if (strpos($connection_type, 'NBN')!==false ){
															 echo ('3');
														 } else{
															 echo ('4');
														 }
                                                    ?>">
                                                        <div class="pricing-box <?php connectionType($connection_type);?>">
                                                            <div class="pricing-title">
                                                                <h3><?php echo($connection_type);?> <?php echo($plan_title);?></h3>
                                                            </div>
                                                            <div class="pricing-price">
                                                            	<?php if ((strpos($connection_type, 'EFM')!==false ) || (strpos($connection_type, 'MBE')!==false )){
																	?><span class="startingFrom">Starting from</span>
																<?php }?>
                                                                <span class="price-unit">&dollar;</span><?php echo($plan_monthly_cost);?><span class="price-tenure">/mo</span>
                                                            </div>
                                                            <div class="pricing-mincost">
                                                            	<span><strong>Available terms:</strong> </span><br/>

                                                                <?php
																//loops through term plans
																foreach ($plan_terms as $term){
																	echo ($term . " months ");
																	?><span>min cost $<?php echo(number_format($plan_setup_cost + ($plan_monthly_cost * $term)));?> </span><br/><?php
																}?>
                                                            </div>
                                                            <div class="pricing-features">
                                                                <ul>
                                                                    <li><strong><?php echo($plan_data);?></strong> Data</li>
                                                                    <li><strong><?php if (empty($plan_min_download)==false ){
																			 echo($plan_min_download);?>/<?php echo($plan_min_upload) ?> - <?php ;
																		 }
																		 echo($plan_max_download);?>/<?php echo($plan_max_upload);?> </strong> Mbps download/upload</li>

                                                                    <?php if (empty($plan_cis)==false ){
																			 ?>  <li><a href="<?php echo($plan_cis['url']);?>">Critical Information Summary (PDF)</a></li> <?php ;
																		 } ?>

                                                                </ul>
                                                            </div>
                                                            <div class="pricing-action">
                                                               	<a href="#planSignup" data-scrollto="#planSignup" class="plan-select button" data-plantitle="<?php echo $plan_title; ?>" data-planid="<?php echo $plan; ?>" data-plantype="<?php echo($connection_type . " " . $plan_data. " " . $plan_max_upload);?>">Select plan</a>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php }?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php }?>
                                        <!--<div id="modemSelection" class="row hidden">
                                            <h3 class="center"> 3. Select a modem</h3>

                                            <div class="row modem-selector bottommargin-sm">
                                                <div class="alert alert-info col_three_fourth nomargin">
                                                    <input type="radio" name="modem-option" id="modem-option1"/><label for="modem-option1">Awesome nifty NBN ready router</label>
                                                </div>
                                                <div class="alert alert-dark col_one_fourth col_last nomargin">
                                                    <p><sup>$</sup>79</p>
                                                </div>
                                            </div>
                                            <div class="row modem-selector bottommargin-sm">
                                                <div class="alert alert-info col_three_fourth nomargin">
                                                    <input type="radio" name="modem-option" id="modem-option2"/><label for="modem-option2">BYO Modem</label>
                                                </div>
                                                <div class="alert alert-dark col_one_fourth col_last nomargin">
                                                    <p><sup>$</sup>0</p>
                                                </div>
                                            </div>
                                        </div>-->
                                       	<div id="planSignup" class="row hidden">
                                          <h3 class="center">2. Make an enquiry</h3>
                                          <div class="alert-dark alert nomargin nopadding clearfix">
                                              <div class="col_half nobottommargin">
                                                  <div class="pricing nobottommargin clearfix">
                                                      <div class="col-full notopmargin">

                                                             <div class="pricing-box nomargin">
                                                             <!-- Will fill with details on click of plan from Select Plan button via jQuery-->

                                                              </div>
                                                        </div>
                                                    </div>
                                              </div>

                                              <div class="col_half col_last nobottommargin notopmargin modal-padding">

                                                  	<div id="signup-step1">
                                                      <h4 class="center">Enter your details</h4>
                                                      <form id="planSelector" action="#" method="post" class="nobottommargin" data-target="<?php echo admin_url('admin-ajax.php'); ?>">
                                                      	  <div class="form-process"></div>
                                                          <div class="clearfix bottommargin-sm">
                                                              <input type="text" class="sm-form-control required" id="name-field" name="name" value="" placeholder="Name">
                                                          </div>

                                                          <div class="clearfix bottommargin-sm">
                                                              <input type="email" class="sm-form-control required" id="email-field" name="email" value="" placeholder="Email">
                                                          </div>

                                                          <div class="clearfix bottommargin-sm">
                                                              <input type="phone" class="sm-form-control required" id="phone-field" name="phone" value="" placeholder="Phone">
                                                          </div>
                                                          <div class="clearfix bottommargin-sm">
                                                              <input type="address" class="sm-form-control required" id="address-field" name="address" value="" placeholder="Address">
                                                          </div>
                                                          <input type="hidden" id="planid" name="planid" value=""  />

                                                          <div class="col_full" id="form-condition-submit">
                                                              <a class="submit button nomargin pull-right arrow dark">Enquire now</a>
                                                          </div>
                                                      </form>
                                                  </div>

                                                 <!-- <div id="signup-step2" class="hidden">
                                                      <h4>Confirm your details</h4>

                                                      <dl>
                                                          <dt>Name</dt>
                                                          <dd>Greg Hollander</dd>

                                                          <dt>Email</dt>
                                                          <dd>GregH1@gmail.com</dd>

                                                          <dt>Phone</dt>
                                                          <dd>0400 000 000</dd>

                                                          <dt>Plan</dt>
                                                          <dd>NBN Lite</dd>

                                                          <dt>Service address</dt>
                                                          <dd>Level 1, 123 Test st TESTVILLE VIC 3133</dd>
                                                      </dl>
                                                      <div class="col_full" id="form-condition-submit3">
                                                          <a class="button nomargin pull-right arrow dark">Confirm signup</a>
                                                      </div>
                                                  </div>-->
                                                  <div id="signup-step2" class="hidden">
                                                      <h4>Thanks for your enquiry</h4>

                                                      <!--p>A confirmation has been sent to <span id="emailAddress">your email address</span>.</p-->
                                                      <p>A consultant will be in touch shortly to finalise your account and setup your connection.
Problems? Call us on <a href="tel:1300 080 820">1300 080 820</a>.</p>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                    </div>
                                </div>
                            </div>
						</form>
				</div>
			</div>
		</section><!-- #content end -->
	</div><!-- #wrapper end -->



	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/functions.js"></script>
